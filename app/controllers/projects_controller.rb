class ProjectsController < ApplicationController
  
  helper_method :sort_column, :sort_direction


  def show
    id = params[:id] # retrieve project task ID from URI route
    @project = Project.find(id) # look up project task by unique ID
    # will render app/views/projects/show.<extension> by default
  end

  def index
    @projects = Project.order(sort_column + " " + sort_direction)
    if  params[:users]
      @projects = @projects.where(user: params[:users].map{|k, v| k})
    end
    @users = Project.pluck(:user).uniq

   # binding.pry
  end

  def new
    # default: render 'new' template
  end

  def create
    @project = Project.create!(params[:project])
    flash[:notice] = "#{@project.title} was successfully created."
    redirect_to projects_path
  end

  def edit
    @project = Project.find params[:id]
  end

  def update
    @project = Project.find params[:id]
    @project.update_attributes!(params[:project])
    flash[:notice] = "#{@project.title} was successfully updated."
    redirect_to project_path(@project)
  end

  def destroy
    @project = Project.find(params[:id])
    @project.destroy
    flash[:notice] = "Project '#{@project.title}' deleted."
    redirect_to projects_path
  end

protected 

  def sort_column
    Project.column_names.include?(params[:sort]) ? params[:sort] : "title"
  end

  def sort_direction
    %w[asc desc].include?(params[:direction]) ? params[:direction] : "asc"
  end





end
